<?php 

	/**
	 * Nirvana blog engine
	 * @author: m.chernobrov@yandex.ru
	 * @since : 02.11.2012
	 */

	use application\Config;

	use engine\Application;	
	use engine\Template;	
	use engine\Models;
	use engine\Router;


	//include loader
	include_once "nirvana/loader.php";
	
	//create application
	$nirvana 				= new Application;
	//load configuration
	$nirvana->config 		= Config::get();
	//load models
	$nirvana->models 		= new Models($nirvana);
	//load template engine
	$nirvana->template 		= new Template($nirvana);
	//load system router
	$nirvana->router 		= new Router($nirvana);
	//get current controller
	$nirvana->router->getController();
	//start application
	$nirvana->router->start();