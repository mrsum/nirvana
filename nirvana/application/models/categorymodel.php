<?php namespace application\models;
	
	use external\orm\ORM;

	/**
	 *
	 */
	class categoryModel extends ORM {

		public $relations = array();

 		public function __construct() {
        	return parent::__construct('category');
    	}

    	public static function load($findme) {
        	return parent::load('category', $findme);
    	}
	}