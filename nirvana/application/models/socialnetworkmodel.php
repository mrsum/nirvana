<?php namespace application\models;
	
	use external\orm\ORM;

	class socialNetworkModel extends ORM {
		
		public $relations = array();

 		public function __construct() {
        	return parent::__construct('social_networks');
    	}

    	public static function load($findme) {
        	return parent::load('social_networks', $findme);
    	}

	}