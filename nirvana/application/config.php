<?php namespace application;
	
	/**
	 * Config
	 */
	class Config {

		public static function get(){

			define('_DB_SERVER_', 'localhost');
			define('_DB_LOGIN_', 'root');
			define('_DB_NAME_', 'nirvana');
			define('_DB_MDP_', '');
			define('_DB_PREFIX_', '');

			return array(
				"resource" 	=> array(
					"sitename" 	=> "nirvana blog",
					"encode" 	=> "utf-8"
				),
				"template" 	=> array(
					//Базовый URL
					"base_url"		=> 	false,
					"debug" 		=> 	true,
					"php_enabled"	=> 	false,
					"tpl_ext"		=> 	"tpl",
					"tpl_dir"		=> 	__DIR__."/views/",
					"cache_dir"		=> 	__DIR__."/views/cache/"
				)
			);
		}

	}