<?php namespace engine;
	
	/**
	 * Application register
	 * @author: m.chernobrov@yandex.ru
	 * @since : 02.11.2012
	 */
	class Application {

		/*
		 * Массив даных передаваемый в регистратор
		 */
		private $vars 		= array();

		/**
		 * Set variables
		 * @param 	string 	$index 	- identificator
		 * @param 	mixed 	$value 	- other data
		 * @return 	void 			
		 *
		 */
		public function __set($index, $value){
			$this->vars[$index] = $value;
		}

		/**
		 * Get variable from registered data
		 * @param 	mixed 	$index 	- identificator
		 * @return 	mixed 			- date from data array by ids
		 */
		public function __get($index){
			return $this->vars[$index];
		}

		public function __construct(){}
	
	}