<?php namespace engine;

	class Router {

		/**
		 * Register
		 */
	    private $nirvana;
		
		/**
		 * Loaded controller
		 * @var 	string 
		 */
	    public $controller 	= "index";
		
		/**
		 * Loaded action
		 * @var 	string
		 */
		public $action 		= "index";
		
		/**
		 * Callable type
		 * @var 	string
		 */
		public $type 		= "web";
	    
		/**
		 * Arguments
		 * @var 	array
		 */
	    public $args 		= array();
		
		/**
		 * Конструктор
		 */
	    function __construct($nirvana) {
	        $this->nirvana = $nirvana;
	    }
		
		/**
		 * Получим данные на основе запроса к системе
		 */
		public function getController(){
			//Получим значение route
			$route = (empty($_GET['route'])) ? '' : $_GET['route'];
			//Если в роутере пусто - положим индексный контроллер
			if (empty($route)) { $route = 'index'; }
			//Разберем на составляющие
			$route = trim($route, '/\\');
	        $parts = explode('/', $route);
	
			foreach ($parts as $key => $value) {
				switch ($key) {
					//нулевой элемент в контроллер
					case 0 : $this->controller 	= $value; break;
					//первый элемент в экшн
					case 1 : $this->action 		= $value; break;
					//Все остальное в аргументы
					default: $this->args[] 		= $value;
				}	
			}
			
			//Проверим вызов на наличие ajax запроса 
			//$request = $this->nirvana->mao->getClass('httpRequest');
			//if($request->getIsAjaxRequest() && !$request->getParam("is_ajax")){
			  	//$this->type = "ajax";
			//}
			
			//гет параметры пожалуй тоже в аргументы запишем
			$this->args = array_merge($this->args, $_GET);
		}
		
		/**
		 * Вызов необходимого контроллера с аргументами
		 */
		public function start(){
			//Соберем имя контроллера
			$class 		= "application\controllers\\".$this->type."\\".$this->controller."controller";
			
			//Создадим экземпляр этого класса
			try {
				$controller = new $class($this->nirvana);
			} catch (\Exception $e){
				$error = new webExceptions($this->nirvana);
				//Метод не найден, ошибка 404
				$error->code404();
			}
			
			//Проверим, можно ли вызвать данный метод?
			if (is_callable(array($controller, $this->action)) == true){
				$controller->{$this->action}();
			} else {
				$error = new webExceptions($this->nirvana);
				//Метод не найден, ошибка 404
				$error->code404();
			}
		}


	}

