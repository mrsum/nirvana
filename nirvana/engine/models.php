<?php namespace engine;
	
	/**
	 *
	 */
	class Models {

		/**
		 * Тут должен быть доступен регистр
		 * @var object
		 */
		private $nirvana;

		/*
		 * @Variables array
		 * @access private
		 */
		private $vars 			= array();

		/**
		 * @constructor
		 * @return void
		 */
		function __construct($nirvana) {
			$this->nirvana 		= $nirvana;
		}

		/**
		 * Установка переменных объекта
		 * @param 	string 	$index
		 * @param 	mixed 	$value
		 * @return 	void
		 */
		public function __set($index, $value){
	    	$this->vars[$index] = $value;
		}

		/**
		 * Получение переменной из зарегистрированных данных
		 * @param 	mixed 	$index 	- идентификатор
		 * @return 	mixed 			- данные по идентификатору
		 */
		public function __get($index){
			return $this->vars[$index];
		}
	    
	    /**
	     * Импорт модуля с разделителем точка
	     * @param type $packageName 
	     */
	    public function import($package){
			$packageArray 		= explode(".", $package);
			$packageName 		= end($packageArray);
			$packagePath 		= implode($packageArray, "\\")."Model";
	        $this->__set($packageName, new $packagePath($this->nirvana));
	    }

	}