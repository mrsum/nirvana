<?php namespace engine;
	
	use external\rain\RainTpl;

	class Template {

		/**
		 * Объект для определения шаблонизатора
		 * @var object
		 */
		public $engine;

		/**
		 * Тут должен быть доступен регистр
		 * @var object
		 */
		private $nirvana;

		/*
		 * @Variables array
		 * @access private
		 */
		private $vars 			= array();

		/**
		 * @constructor
		 * @return void
		 */
		function __construct($nirvana) {
			$this->nirvana 		= $nirvana;
		}

		/**
		 * Установка переменных объекта
		 * @param 	string 	$index
		 * @param 	mixed 	$value
		 * @return 	void
		 */
		public function __set($index, $value){
	    	$this->vars[$index] = $value;
		}
		
		/**
		 * Инициализация шаблонизатора
		 */
		public function loadTemplateEngine(){
			$retValue = true;
			try {
				//После того, как объект шаблонов привязан к системе, применим его конфигурацию		
				if(count($this->nirvana->template) > 0){
					//Вызовем статичный класс
					RainTpl::configure($this->nirvana->config["template"]);
				}
				$this->engine = new RainTpl($this->nirvana);	

			} catch (\Exception $e) {
				$retValue = false;
			}
			return $retValue;
		}	

		/**
		 * Метод обертка прикрепления данных к шаблону
		 * @param 	string 	$variable 			- ключ
		 * @param 	mixed 	$value 				- значение
		 * @return 	void
		 */
		public function assign($variable, $value = null){
			$this->nirvana->template->engine->assign($variable, $value);
		}

		/**
		 * Метод обертка отрисовки шаблона
		 * @param 	string 	$tpl_name 		- название шаблона, допустимо указывать в формате folder/file
		 * @param 	mixed 	$return_string 	- возвращаемое значение
		 * @return 	void
		 */
		public function draw($tplName, $returnString = false){
			$this->nirvana->template->engine->draw($tplName, $returnString);	
		}
		
		/**
		 * Если доступен валидный по дате кэш, то он возвратится
		 *
		 * @param 	string 	$tpl_name 			- Имя возвращаемого шаблона
		 * @param 	int 	$expiration_time 	- Время жизни шаблона, если кэш находится в рамках этого времени то он и возвратится
		 * @return 	string 
		 */
		public function cache($tpl_name, $expire_time = null, $cache_id = null ){
			$this->nirvana->template->engine->cache($tpl_name, $expire_time = null, $cache_id = null);
		}

	}