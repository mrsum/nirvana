<?php namespace external\orm;
	
	use external\orm\Db;

	class ORM {

	    public 		$id;
	    protected 	$v = array();
		protected 	$vDescribe = array();
	    protected 	$vmax = array();
	    protected 	$table;
	    protected 	$key;
		public 		$relations;
	   
	    public function __construct($table) {
	        $this->table = _DB_PREFIX_.$table;
	        $result_fields = Db::inst()->query('DESCRIBE '.$this->table);
			while ($row_field = $result_fields->fetch()) {
				
	            $this->v[$row_field['Field']] = '';

				preg_match('#\(+(.*)\)+#', $row_field['Type'], $result);
				if (array_key_exists(1, $result)) $this->vDescribe[$row_field['Field']]['size'] = $result[1];
				
				$this->vDescribe[$row_field['Field']]['type'] = substr($row_field['Type'], 0, strpos($row_field['Type'], '('));
				
	            if($row_field['Key']=='PRI') $this->key = $row_field['Field'];
				
	        }
	        return $this->key ? true : false;
	    }
		
	    public static function load($table, $findme) {
	    	$calledClass = get_called_class();
	    	$obj = new $calledClass($table);
			$params = is_int($findme) ? $obj->key.'='.$findme : $findme;
	        $obj->v = Db::inst()->getRow('*', $obj->table,  $params);
			$obj->id = $obj->v[$obj->key];
			$obj->refreshRelation();
			return $obj;
	    }
		
		public function refreshRelation() {
			if(!empty($this->relations)) {
				foreach($this->relations as $relation) {
					$this->vmax[$relation['table']] = ORM::load($relation['table'], $this->__get($relation['field']));
				}
			}
		}
	 
	    public function insert() {
	        $this->id = Db::inst()->insert($this->table, $this->v);
	    }
	   
	    public function update() {
	        Db::inst()->update($this->table, $this->v, $this->key.'='.$this->id);
	    }
	   
	    public function delete() {
	        Db::inst()->delete($this->table, $this->key.'='.$this->id);
	    }

	    public function save() {
	        return $this->id ? $this->update() : $this->insert();
	    }

	    public function __set($key, $value) {
			$numericTypes = array('float', 'int', 'tinyint');
			$intTypes = array('int', 'tinyint');
			$dateTypes = array('date', 'datetime');
	    	$testMethod = 'set'.ucfirst($key);
			$calledClass = get_called_class();
	    	if(method_exists($calledClass, $testMethod))  $value = $calledClass::$testMethod($value);
			try {
				if(strlen($value) > $this->vDescribe[$key]['size']) {
					throw new Exception('"'.$key.'" value is too long ('.$this->vDescribe[$key]['size'].')');
				}
				if(in_array($this->vDescribe[$key]['type'], $numericTypes)) {
					if(!is_numeric($value)) {
						throw new Exception('"'.$key.'" value should be numeric');
					} elseif(!is_int($value) && in_array($this->vDescribe[$key]['type'], $intTypes)) {
						throw new Exception('"'.$key.'" value should be int');
					}
				}
				if(in_array($this->vDescribe[$key]['type'], $dateTypes) && mb_substr_count($value, "-") != 3) {
					throw new Exception('"'.$key.'" value should be date');
				}
			}
			catch(Exception $e) {
				echo $e->getMessage().'<br/>';
			}
			$this->v[$key] = $value;			
	    }
		   
	    public function __get($key) {
	        $value = array_key_exists($key, $this->vmax) ? $this->vmax[$key] : $this->v[$key];
			$testMethod = 'get'.ucfirst($key);
			if(method_exists(get_called_class(), $testMethod))  $value = self::$testMethod($value);
			return $value;
	    }
		
		public function hydrate($values) {
			foreach($values as $key => $value) {
				$this->__set($key, $value);
			}
		}
	}