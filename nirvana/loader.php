<?php 

	/**
	 * SPL autoload
	 * @author: m.chernobrov@yandex.ru
	 * @since : 02.11.2012
	 */

	/* show errors */
	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	/* starting point */
	ini_set('include_path', dirname(__FILE__).DIRECTORY_SEPARATOR);

	spl_autoload_register(function ($class) {

		$fileName = __DIR__."/".strtolower(str_replace("\\", "/", $class)).".php";
	
	    if(file_exists($fileName))
	 		include $fileName;
		else 
	 		throw new \Exception("File not exists [".$fileName."]", 1);
	});